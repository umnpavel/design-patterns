<?php

namespace App\DesignPatterns\CreationalPatterns\FactoryMethod\Classes;

use App\DesignPatterns\CreationalPatterns\FactoryMethod\Abstract\SocialNetworkPoster;
use App\DesignPatterns\CreationalPatterns\FactoryMethod\Interfaces\SocialNetworkConnector;

/**
 * Этот Конкретный Создатель поддерживает Facebook. Помните, что этот класс
 * также наследует метод post от родительского класса. Конкретные Создатели —
 * это классы, которые фактически использует Клиент.
 */
class VkPoster extends SocialNetworkPoster
{
    private string $password;
    private string $login;

    public function __construct(string $login, string $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    public function getSocialNetwork(): SocialNetworkConnector
    {
        return new VkConnector($this->login, $this->password);
    }
}
