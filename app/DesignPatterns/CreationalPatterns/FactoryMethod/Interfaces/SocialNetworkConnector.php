<?php

namespace App\DesignPatterns\CreationalPatterns\FactoryMethod\Interfaces;

/**
 * Интерфейс Продукта объявляет поведения различных типов продуктов.
 */
interface SocialNetworkConnector
{
    public function logIn(): void;

    public function logOut(): void;

    public function createPost($content): void;
}
