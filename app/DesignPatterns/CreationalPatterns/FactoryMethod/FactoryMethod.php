<?php

namespace App\DesignPatterns\CreationalPatterns\FactoryMethod;

use App\DesignPatterns\CreationalPatterns\FactoryMethod\Abstract\SocialNetworkPoster;

class FactoryMethod
{
    static function clientCode(SocialNetworkPoster $creator): void
    {
        $creator->post("Hello world!");
    }
}
