<?php

namespace App\DesignPatterns\CreationalPatterns\AbstractFactory;

use App\DesignPatterns\CreationalPatterns\AbstractFactory\Interfaces\Furniture\FurnitureFactory;
use App\DesignPatterns\CreationalPatterns\FactoryMethod\Abstract\SocialNetworkPoster;

class AbstractFactory
{
    /**
     *  Клиентский код работает с фабриками и продуктами только через абстрактные
     *  типы: Абстрактная Фабрика и Абстрактный Продукт. Это позволяет передавать
     *  любой подкласс фабрики или продукта клиентскому коду, не нарушая его.
     *
     *
     * @param FurnitureFactory $factory
     * @return void
     */
    static function clientCode(FurnitureFactory $factory): void
    {
        $armchair = $factory->createArmchair();
        $sofa = $factory->createSofa();
        $table = $factory->createTable();

        echo $armchair->getString() . PHP_EOL;
        echo $sofa->getString() . PHP_EOL;
        echo $table->getString() . PHP_EOL;
    }
}
