<?php

namespace App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Furniture;

use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product\ModernArmchair;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product\ModernSofa;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product\ModernTable;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Interfaces\Furniture\FurnitureFactory;

/**
 * Каждая Конкретная Фабрика соответствует определённому варианту (или
 * семейству) продуктов.
 *
 * Эта Конкретная Фабрика создает шаблоны Modern.
 *
 * Конкретная Фабрика производит семейство продуктов одной вариации. Фабрика
 * гарантирует совместимость полученных продуктов. Обратите внимание, что
 * сигнатуры методов Конкретной Фабрики возвращают абстрактный продукт, в то
 * время как внутри метода создается экземпляр конкретного продукта.
 */
class ModernFurnitureFactory implements FurnitureFactory
{

    /**
     * @return ModernArmchair
     */
    public function createArmchair(): ModernArmchair
    {
        return new ModernArmchair();
    }

    /**
     * @return ModernSofa
     */
    public function createSofa(): ModernSofa
    {
        return new ModernSofa();
    }

    /**
     * @return ModernTable
     */
    public function createTable(): ModernTable
    {
        return new ModernTable();
    }
}
