<?php

namespace App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Furniture;

use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product\VictorianArmchair;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product\VictorianSofa;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product\VictorianTable;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Interfaces\Furniture\FurnitureFactory;

/**
 * Каждая Конкретная Фабрика соответствует определённому варианту (или
 * семейству) продуктов.
 *
 * Эта Конкретная Фабрика создает шаблоны Victorian.
 *
 * Конкретная Фабрика производит семейство продуктов одной вариации. Фабрика
 * гарантирует совместимость полученных продуктов. Обратите внимание, что
 * сигнатуры методов Конкретной Фабрики возвращают абстрактный продукт, в то
 * время как внутри метода создается экземпляр конкретного продукта.
 */
class VictorianFurnitureFactory implements FurnitureFactory
{

    /**
     * @return VictorianArmchair
     */
    public function createArmchair(): VictorianArmchair
    {
        return new VictorianArmchair();
    }

    /**
     * @return VictorianSofa
     */
    public function createSofa(): VictorianSofa
    {
        return new VictorianSofa();
    }

    /**
     * @return VictorianTable
     */
    public function createTable(): VictorianTable
    {
        return new VictorianTable();
    }
}
