<?php

namespace App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Furniture;

use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product\ArtDecoArmchair;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product\ArtDecoSofa;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product\ArtDecoTable;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Interfaces\Furniture\FurnitureFactory;

/**
 * Каждая Конкретная Фабрика соответствует определённому варианту (или
 * семейству) продуктов.
 *
 * Эта Конкретная Фабрика создает шаблоны Art-Deco.
 *
 * Конкретная Фабрика производит семейство продуктов одной вариации. Фабрика
 * гарантирует совместимость полученных продуктов. Обратите внимание, что
 * сигнатуры методов Конкретной Фабрики возвращают абстрактный продукт, в то
 * время как внутри метода создается экземпляр конкретного продукта.
 */
class ArtDecoFurnitureFactory implements FurnitureFactory
{

    /**
     * @return ArtDecoArmchair
     */
    public function createArmchair(): ArtDecoArmchair
    {
        return new ArtDecoArmchair();
    }

    /**
     * @return ArtDecoSofa
     */
    public function createSofa(): ArtDecoSofa
    {
        return new ArtDecoSofa();
    }

    /**
     * @return ArtDecoTable
     */
    public function createTable(): ArtDecoTable
    {
        return new ArtDecoTable();
    }
}
