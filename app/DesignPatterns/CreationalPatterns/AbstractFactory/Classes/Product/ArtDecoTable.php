<?php

namespace App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product;

use App\DesignPatterns\CreationalPatterns\AbstractFactory\Interfaces\Product\Table;

class ArtDecoTable implements Table
{
    /**
     * @return string
     */
    public function getString(): string
    {
        return 'created ArtDeco Table';
    }
}
