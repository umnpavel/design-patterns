<?php

namespace App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product;

use App\DesignPatterns\CreationalPatterns\AbstractFactory\Interfaces\Product\Armchair;

class ArtDecoArmchair implements Armchair
{
    /**
     * @return string
     */
    public function getString(): string
    {
        return 'created ArtDeco Armchair';
    }
}
