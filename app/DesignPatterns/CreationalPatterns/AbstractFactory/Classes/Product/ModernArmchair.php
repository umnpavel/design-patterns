<?php

namespace App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product;

use App\DesignPatterns\CreationalPatterns\AbstractFactory\Interfaces\Product\Armchair;

class ModernArmchair implements Armchair
{
    /**
     * @return string
     */
    public function getString(): string
    {
        return 'created Modern Armchair';
    }
}
