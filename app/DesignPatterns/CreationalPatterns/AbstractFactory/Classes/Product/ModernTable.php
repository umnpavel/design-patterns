<?php

namespace App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product;

use App\DesignPatterns\CreationalPatterns\AbstractFactory\Interfaces\Product\Table;

class ModernTable implements Table
{
    /**
     * @return string
     */
    public function getString(): string
    {
        return 'created Modern Table';
    }
}
