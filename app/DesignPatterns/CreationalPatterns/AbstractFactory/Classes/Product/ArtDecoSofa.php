<?php

namespace App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Product;

use App\DesignPatterns\CreationalPatterns\AbstractFactory\Interfaces\Product\Sofa;

class ArtDecoSofa implements Sofa
{
    /**
     * @return string
     */
    public function getString(): string
    {
        return 'created ArtDeco Sofa';
    }
}
