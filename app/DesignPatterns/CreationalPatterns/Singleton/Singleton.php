<?php

namespace App\DesignPatterns\CreationalPatterns\Singleton;

use App\DesignPatterns\CreationalPatterns\Singleton\Classes\SingletonBase;

class Singleton
{
    /**
     * Клиентский код.
     */
    static function clientCode()
    {
        $s1 = SingletonBase::getInstance();
        $s2 = SingletonBase::getInstance();
        if ($s1 === $s2) {
            echo "Синглтон работает, обе переменные содержат один и тот же экземпляр.";
        } else {
            echo "Ошибка синглтона, переменные содержат разные экземпляры.";
        }
    }
}
