<?php

namespace App\DesignPatterns\CreationalPatterns\Prototype;

use App\DesignPatterns\CreationalPatterns\Prototype\Classes\Author;
use App\DesignPatterns\CreationalPatterns\Prototype\Classes\Page;

class Prototype
{
    /**
     * Клиентский код.
     */
    static function clientCode()
    {
        $author = new Author('Мэтт Зандстра');
        $page = new Page('Совет дня', 'Предпочитайте композицию наследованию.', $author);


        $page->addComment('Хороший совет, спасибо!');

        $draft = clone $page;
        echo "Дамп клона. Обратите внимание, что автор теперь ссылается на два объекта.\n\n";
        print_r($draft);
    }
}
