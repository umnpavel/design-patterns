<?php

namespace App\DesignPatterns\CreationalPatterns\Builder;

use App\DesignPatterns\CreationalPatterns\Builder\Classes\ConcreteBuilders\ConcreteBuilder1;
use App\DesignPatterns\CreationalPatterns\Builder\Classes\Directors\Director;

class Builder
{
    /**
     * Клиентский код создаёт объект-строитель, передаёт его директору, а затем
     * инициирует процесс построения. Конечный результат извлекается из объекта-
     * строителя.
     */
    static function clientCode(Director $director)
    {
        $builder = new ConcreteBuilder1();
        $director->setBuilder($builder);

        echo "Standard basic product: </br>";
        $director->buildMinimalViableProduct();
        $builder->getProduct()->listParts();

        echo "Standard full featured product: </br>";
        $director->buildFullFeaturedProduct();
        $builder->getProduct()->listParts();

        // Помните, что паттерн Строитель можно использовать без класса Директор.
        echo 'Custom product: </br>';
        $builder->producePartA();
        $builder->producePartC();
        $builder->getProduct()->listParts();
    }
}
