<?php

namespace App\Http\Controllers;

use App\DesignPatterns\CreationalPatterns\FactoryMethod\Classes\FacebookPoster;
use App\DesignPatterns\CreationalPatterns\FactoryMethod\Classes\VkPoster;
use App\DesignPatterns\CreationalPatterns\FactoryMethod\FactoryMethod;

class FactoryMethodController extends Controller
{

    /**
     * @return void
     *
     * php artisan tinker
     * app()->call('App\Http\Controllers\FactoryMethodController@index');
     */
    public function index()
    {
        echo "Testing ConcreteCreator1:" . PHP_EOL;
        FactoryMethod::clientCode(new FacebookPoster("john_smith", "******"));

        echo "Testing ConcreteCreator2:" . PHP_EOL;
        FactoryMethod::clientCode(new VkPoster("john_smith@example.com", "******"));
    }
}
