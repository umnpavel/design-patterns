<?php

namespace App\Http\Controllers;

use App\DesignPatterns\CreationalPatterns\AbstractFactory\AbstractFactory;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Furniture\ArtDecoFurnitureFactory;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Furniture\ModernFurnitureFactory;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Furniture\VictorianFurnitureFactory;
use App\DesignPatterns\CreationalPatterns\Prototype\Prototype;
use App\DesignPatterns\CreationalPatterns\Singleton\Singleton;


class SingletonController extends Controller
{

    /**
     * Клиентский код
     */
    public function index()
    {
        Singleton::clientCode();
    }
}
