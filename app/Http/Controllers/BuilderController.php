<?php

namespace App\Http\Controllers;

use App\DesignPatterns\CreationalPatterns\Builder\Builder;
use App\DesignPatterns\CreationalPatterns\Builder\Classes\Directors\Director;


class BuilderController extends Controller
{

    /**
     * Клиентский код может работать с любым конкретным классом фабрики.
     */
    public function index()
    {
        Builder::clientCode(new Director());
    }
}
