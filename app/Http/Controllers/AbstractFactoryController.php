<?php

namespace App\Http\Controllers;

use App\DesignPatterns\CreationalPatterns\AbstractFactory\AbstractFactory;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Furniture\ArtDecoFurnitureFactory;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Furniture\ModernFurnitureFactory;
use App\DesignPatterns\CreationalPatterns\AbstractFactory\Classes\Furniture\VictorianFurnitureFactory;


class AbstractFactoryController extends Controller
{

    /**
     * Клиентский код может работать с любым конкретным классом фабрики.
     */
    public function index()
    {
        echo "Factory Art Deco Furniture:" . PHP_EOL;
        AbstractFactory::clientCode(new ArtDecoFurnitureFactory());
        echo '------------' . PHP_EOL;

        echo "Factory Modern Furniture:" . PHP_EOL;
        AbstractFactory::clientCode(new ModernFurnitureFactory());
        echo '------------' . PHP_EOL;

        echo "Factory Victorian Furniture:" . PHP_EOL;
        AbstractFactory::clientCode(new VictorianFurnitureFactory);
        echo '------------' . PHP_EOL;
    }
}
