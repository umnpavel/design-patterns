<?php

use App\Http\Controllers\AbstractFactoryController;
use App\Http\Controllers\FactoryMethodController;
use App\Http\Controllers\PrototypeController;
use App\Http\Controllers\SingletonController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'creational-patterns'], function () {
    Route::get('/factory-method', [FactoryMethodController::class, 'index']);
    Route::get('/abstract-method', [AbstractFactoryController::class, 'index']);
    Route::get('/prototype', [PrototypeController::class, 'index']);
    Route::get('/singleton', [SingletonController::class, 'index']);
});


